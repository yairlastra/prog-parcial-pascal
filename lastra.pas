//NOTA: 3
program lastra;
uses crt;
const
  min=1;
  max_f=10;
  max_c_v=7;
  max_c_p=4;
  max_c_dni=2;
type
  tm_v=array[min..max_f,min..max_c_v] of integer;
  tm_p=array[min..max_f,min..max_c_p] of integer;
  tm_dni=array[min..max_f,min..max_c_dni] of integer;
var
  matriz_vuelos:tm_v;
  matriz_pasajeros:tm_p;
  max_filas:integer;
  f1,f2:integer;
  numero_vuelo_cambio:integer;
  ciudad_origen,ciudad_destino:integer;
procedure cargar_matriz_vuelos(var m_aux1:tm_v;var x:integer);
var nv:integer;
begin
  writeln('Ingresar numero de vuelo');
  readln(nv);
  x:=0;
  while nv<>0 do
  begin
        x:=x+1;
        m_aux1[x,1]:=nv;
        writeln('Ingresar numero de avion');
        readln(m_aux1[x,2]);
        writeln('Ingresar fecha de partida en formato AAAAMMDD');
        readln(m_aux1[x,3]);
        writeln('Ingresar fecha de llegada en formato AAAAMMDD');
        readln(m_aux1[x,4]);
        writeln('Ingresar codigo de ciudad de origen');
        readln(m_aux1[x,5]);
        writeln('Ingresar codigo de ciudad de destino');
        readln(m_aux1[x,6]);
        writeln('Ingresar estado del vuelo');
        readln(m_aux1[x,7]);
        writeln('Ingresar numero de vuelo');
        readln(nv)
  end;
end;
procedure cargar_matriz_pasajeros(var m_aux2:tm_p);
var
  nv,y:integer;
begin
  writeln('Ingresar numero de vuelo');
  readln(nv);
  y:=0;
  while nv<>0 do
  begin
        y:=y+1;
        m_aux2[y,1]:=nv;
        writeln('Ingresar numero de documento');
        readln(m_aux2[y,2]);
        writeln('Ingresar numero de asiento');
        readln(m_aux2[y,3]);
        writeln('Ingresar importe del pasaje');
        readln(m_aux2[y,4]);
        writeln('Ingresar numero de vuelo');
        readln(nv)
  end;
end;
function importe_total(fecha1:integer;fecha2:integer):integer;
var
   i,r,suma_total:integer;
begin
  writeln('Ingresar primera fecha');
  readln(fecha1);
  writeln('Ingresar segunda fecha');
  readln(fecha2);
  suma_total:=0;
  i:=1;
    //Para recorrer las matrices si sabes cuantos son los registros solo tenes que hacer un for
  while i<>(max_filas+1) do
  begin
		//La condicion debería ser >= sino en ocasiones no da bien el resultad
        if matriz_vuelos[i,3]>fecha1 then
		   //La condicion debería ser <= sino en ocasiones no da bien el resultad
           if matriz_vuelos[i,3]<fecha2 then
              if matriz_vuelos[i,7]=1 then
              begin
                  r:=1;
                  while matriz_vuelos[i,1] <> matriz_pasajeros[r,1] do
                        r:=r+1;
                  suma_total:=suma_total+matriz_pasajeros[r,4]
             end;
        i:=i+1
  end;
  importe_total:=suma_total
end;
procedure cambiar_valores_numero_vuelo(z:integer);
var
   t,k:integer;
begin
  writeln('Ingresar numero de vuelo');
  readln(z);
  t:=1;
  //Para recorrer las matrices si sabes cuantos son los registros solo tenes que hacer un for
  while z<>matriz_vuelos[t,1] do
        t:=t+1;
  k:=1;
    //Para recorrer las matrices si sabes cuantos son los registros solo tenes que hacer un for
  while z<>matriz_pasajeros[k,1] do
        k:=k+1;

  matriz_vuelos[t,7]:= -1;
  //Hay que cambiar todos los asientos de ese vuelo y ponerlos en 0
  matriz_pasajeros[k,3]:=0;
end;
function encontrar_dni(city1:integer;city2:integer):integer;
var
   m_aux3:tm_dni;
   s,w,o,u,maximo,maximo_dni,last:integer;
   busq:integer;
begin
  writeln('Ingresar ciudad de origen');
  readln(city1);
  writeln('Ingresar ciudad de destino');
  readln(city2);
  s:=1;
  o:=1;
  w:=0;
  maximo:=0;
  while matriz_vuelos[s,5]<>(max_filas+1) do
  begin
        if matriz_vuelos[s,5]=city1 then
           if matriz_vuelos[s,6]=city2 then
           begin
                while matriz_vuelos[s,1]<>matriz_pasajeros[o,1] do
                      o:=o+1;
                busq:=1;
                u:=0;
                //ERROR: ROMPE el programa. La matriz m_aux3 viene con valores basura y hace que luego de varios ciclos rompa y nunca llegue al resultado.
                while m_aux3[busq,1]<>matriz_pasajeros[o,1] do
                      busq:=busq+1;
                if m_aux3[busq,1]<>matriz_pasajeros[o,1] then
                begin
                      w:=w+1;
                      m_aux3[w,1]:=matriz_pasajeros[o,2];
                      m_aux3[w,2]:=m_aux3[w,2]+1
                end
                else
                    m_aux3[busq,2]:=m_aux3[busq,2]+1;
           end;
        s:=s+1
  end;
  while busq<>(w+1) do
  begin
        if m_aux3[busq,2]>maximo then
        begin
             maximo:=m_aux3[busq,2];
             maximo_dni:=m_aux3[busq,1]
        end;
        busq:=busq+1;
  end;
  encontrar_dni:=maximo_dni;
end;
begin
  //ERROR: para todos los procedimientos y funciones, NUNCA se pide que ingresen los valores dentro de las funciones y procedimientos.
  //esto lo dijimos enr eiteradas veces, los valores que voy a pasar como parámetro no se piden dentro de los procedimientos o funciones
  //se piden antes de llamar a dichas funciones o procedimientos.

  clrscr;
  //ERROR: Se pedía solo la matriz como valor por refencia.
  cargar_matriz_vuelos(matriz_vuelos,max_filas);
  cargar_matriz_pasajeros(matriz_pasajeros);

  //Deberían pedirse f1 y f2 acá y no adentro de la función.
  //ERROR: nunca se asigna a una variable el resultado de la función. Calcula mal el importe, no maneja bien los índices
  //ERROR: dijimos en clase que dentro de las funciones y procedimientos no se piden las variables, deben pedirse afuera y pasarlas como parámetros
  importe_total(f1,f2);

  //ERROR: El estado lo cambia bien, pero los asientos cambia solo el primero que encuentra.
  //ERROR: dijimos en clase que dentro de las funciones y procedimientos no se piden las variables, deben pedirse afuera y pasarlas como parámetros
  cambiar_valores_numero_vuelo(numero_vuelo_cambio);

  //ERROR: Nunca se asigna el resultado de la función a una variable
  //ERROR: El resultado es incorrecto
  //ERROR: dijimos en clase que dentro de las funciones y procedimientos no se piden las variables, deben pedirse afuera y pasarlas como parámetros
  encontrar_dni(ciudad_origen,ciudad_destino);
  repeat until keypressed
end.
